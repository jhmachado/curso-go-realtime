import {EventEmitter} from 'events';

class Socket {
  constructor() {
    this.webSocket = new WebSocket();
    this.eventEmitter = new EventEmitter();

    ws.onmessage = this.message.bind(this);
    ws.onopen = this.open.bind(this);
    ws.onclose = this.close.bind(this);
  }

  on(name, fn) {
    this.eventEmitter.on(name, fn);
  }

  off(name, fn) {
    this.eventEmitter.removeListener(name, fn);
  }

  emit(name, data) {
    const message = JSON.stringify({name, data});
    this.webSocket.send(message);
  }

  message(e) {
    try {
      const message = JSON.parse(e.data);
      this.eventEmitter.emit(message.name, message.data);
    } catch(err) {
      this.eventEmitter.emit('error', err);
    }
  }

  open() {
    this.eventEmitter.emit('open');
  }

  close() {
    this.eventEmitter.emit('close');
  }
}

export default Socket;

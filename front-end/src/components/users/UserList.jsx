import React, {Component} from 'react';
import PropTypes from 'prop-types';
import User from './User.jsx';

class UserList extends Component {
  render() {
    const usersList = this.props.users.map((user) =>
      <User
        user={user}
        key={user.id}
      />
    );

    return (
      <ul>{usersList}</ul>
    )
  }
}

UserList.propTypes = {
  users: PropTypes.array.isRequired
}

export default UserList;

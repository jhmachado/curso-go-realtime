import React, {Component} from 'react';
import Message from './Message.jsx';
import PropTypes from 'prop-types';

class MessageList extends Component {
  render() {
    const messagesList = this.props.messages.map((message) =>
      <Message message={message} key={message.id} />
    );

    return (
      <ul>{messagesList}</ul>
    )
  }
}

MessageList.propTypes = {
  messages: PropTypes.array.isRequired
}

export default MessageList;

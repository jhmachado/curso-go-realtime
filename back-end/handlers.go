package main

import (
	"fmt"
	"github.com/mitchellh/mapstructure"
)

type Channel struct {
	Id 	 string `json:"id"`
	Name string `json:"name"`
}

func addChannel(client *Client, data interface{}) {
	var channel Channel
	// var message Message

	err := mapstructure.Decode(data, &channel)
	if err != nil {
		client.send <- Message{"error", err.Error()}
		return
	}

	/*send to the database in a go routine*/
	// go func() {
	// }()
	// fmt.Printf("%#v\n", channel)

	// channel.Id = "ABC123"
	// message.Name = "channel add"
	// message.Data = channel
	// client.send <- message
}

func subscribehannel(client *Client, data interface{}) {
	/*re-write this in a go routing*/

	
}
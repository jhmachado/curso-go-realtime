package main

import (
	"net/http"
)

func main() {
	router := NewRouter()

	router.Handle("channel add", addChannel)
	// router.Handle("channel subscribe", subscribehannel)
	// router.Handle("channel unsubscribe", subscribehannel)
	
	// router.Handle("user edit", subscribehannel)
	// router.Handle("user subscribe", subscribehannel)
	// router.Handle("user unsubscribe", subscribehannel)

	// router.Handle("message add", subscribehannel)
	// router.Handle("message subscribe", subscribehannel)
	// router.Handle("message unsubscribe", subscribehannel)

	http.Handle("/", router)
	http.ListenAndServe(":9000", nil)
}
